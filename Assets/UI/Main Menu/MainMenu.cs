﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private Button m_playButton;
    [SerializeField] private Button m_quitButton;

    private void OnEnable()
    {
        m_playButton.onClick.AddListener(OnPlayButtonClicked);
        m_quitButton.onClick.AddListener(OnQuitButtonClicked);
    }

    private void OnDisable()
    {
        m_playButton.onClick.RemoveListener(OnPlayButtonClicked);
        m_quitButton.onClick.RemoveListener(OnQuitButtonClicked);
    }

    private void OnPlayButtonClicked()
    {
        //SceneChanger.instance.FadeToNextLevel(); Sorry Doug. It's for the JAAAAM
    }

    private void OnQuitButtonClicked()
    {
        Application.Quit();
    }
}
