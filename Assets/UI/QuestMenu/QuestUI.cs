﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestUI : MonoBehaviour
{
    // Exposed Variables
    [SerializeField] private GameObject _mainHUD = null;
    [SerializeField] private GameObject _questUI = null;
    [SerializeField] private Text _questDisplayText = null;

    // Private Variables
    private bool _uiOpen = false;

    // Start is called before the first frame update
    void Start()
    {
        _questUI.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Q))
        {
            // Toggle the UI
            _uiOpen = !_uiOpen;

            _mainHUD.SetActive(!_uiOpen);
            _questUI.SetActive(_uiOpen);

            if(_uiOpen == true)
            {
                // Query the current quest information
                _questDisplayText.text = QuestManager.Instance.GetActiveQuestDisplayInformation();
            }
        }
    }
}
