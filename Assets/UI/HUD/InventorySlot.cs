﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class InventorySlot : MonoBehaviour
{
    // Constants
    public const int kIconWidth = 75;
    public const int kIconHeight = 75;

    // Properties
    public Sprite Icon
    {
        get
        {
            return Icon;
        }
        set
        {
            m_icon = value;
            if(m_icon != null)
            {
                m_image.enabled = true;
                m_image.sprite = m_icon;
            }
            else
            {
                m_image.enabled = false;
            }
        }
    }

    public int ItemCount
    {
        get
        {
            return m_itemCount;
        }
        set
        {
            m_itemCount = value;
            if(m_itemCount > 0)
            {
                m_text.text = m_itemCount.ToString();
                m_text.enabled = true;
            }
            else
            {
                m_text.enabled = false;
            }
        }
    }

    public bool Selected
    {
        get
        {
            return m_selected;
        }
        set
        {
            m_selected = value;
            if(m_selected)
            {
                m_backgroundImage.color = new Color(0.1f, 0.9f, 0.1f);
            }
            else
            {
                m_backgroundImage.color = new Color(0.0f, 0.0f, 0.0f);
            }
        }
    }

    // Exposed Variables
    [SerializeField] private Image m_backgroundImage = null;

    // Private Variables
    private Image m_image = null;
    private Sprite m_icon = null;
    private Text m_text = null;
    private int m_itemCount = 0;
    private bool m_selected = false;

    private void Awake()
    {
        m_image = this.gameObject.GetComponent<Image>();
        m_text = this.gameObject.GetComponentInChildren<Text>();

        m_image.enabled = false;
        m_text.enabled = false;
    }
}
