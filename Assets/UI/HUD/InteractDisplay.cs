﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractDisplay : MonoBehaviour
{
    // Private variables
    private Text _text = null;

    // Start is called before the first frame update
    void Start()
    {
        _text = GetComponent<Text>();
        _text.enabled = false;

        EventManager.Instance._OnInteractMessageEvents.Add(OnInteractMessage);
    }

    void OnDestroy()
    {
        if(EventManager.Instance != null)
        {
            EventManager.Instance._OnInteractMessageEvents.Remove(OnInteractMessage);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Events Interface
    public void OnInteractMessage(string msg)
    {
        if(msg == "")
        {
            _text.enabled = false;
        }
        else
        {
            _text.enabled = true;
            _text.text = "E - " + msg;
        }
    }
}
