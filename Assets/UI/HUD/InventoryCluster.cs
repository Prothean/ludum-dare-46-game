﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryCluster : MonoBehaviour
{
    // Constants
    public const int kInventorySlotSpacing = 5;

    // Exposed Variables
    [SerializeField] Text _selectedItemText = null;
    [SerializeField] GameObject _inventorySlotObject;

    // Private Variables
    private InventorySlot[] m_slots;
    private int m_currentlySelectedSlot = 0;
    private bool m_FadeItemDisplayText = false;
    private const float m_FadeSpeed = 0.9f;
    private float m_FadeTimer = m_FadeSpeed;

    private void Start()
    {
        m_slots = new InventorySlot[Inventory.kInventorySlots];

        // Spawn the Inventory Slots
        {
            // Calculate the start position of the slots
            float startXPosition = (float)(Inventory.kInventorySlots / 2) * InventorySlot.kIconWidth + (kInventorySlotSpacing * ((Inventory.kInventorySlots / 2) - 1));
            for(int i = 0; i < Inventory.kInventorySlots; ++i)
            {
                GameObject newSlot = Instantiate(_inventorySlotObject);
                newSlot.transform.SetParent(this.gameObject.transform);

                newSlot.transform.localPosition = new Vector3(startXPosition, 0.0f, 0.0f);
                startXPosition += InventorySlot.kIconWidth + kInventorySlotSpacing;

                m_slots[i] = newSlot.GetComponent<InventorySlot>();

                if(i == m_currentlySelectedSlot)
                {
                    m_slots[i].Selected = true;
                }
            }
        }

        // Register for the item events
        EventManager.Instance._OnItemPickupEvents.Add(OnItemPickedUp);
        EventManager.Instance._OnItemDropEvents.Add(OnItemDropped);
        EventManager.Instance._OnInventoryIndexChangedEvents.Add(OnInventoryIndexChanged);

        _selectedItemText.enabled = false;
    }

    private void Update()
    {
        if(m_FadeItemDisplayText == true)
        {
            m_FadeTimer -= Time.deltaTime;

            Color currentColor = _selectedItemText.color;
            currentColor.a -= m_FadeSpeed * Time.deltaTime;
            _selectedItemText.color = currentColor;

            if(m_FadeTimer <= 0.0f)
            {
                _selectedItemText.enabled = false;
                m_FadeItemDisplayText = false;
            }
        }
    }

    private void OnDestroy()
    {
        // Unregister from the item events
        if(EventManager.Instance != null)
        {
            EventManager.Instance._OnItemPickupEvents.Remove(OnItemPickedUp);
            EventManager.Instance._OnItemDropEvents.Remove(OnItemDropped);
            EventManager.Instance._OnInventoryIndexChangedEvents.Remove(OnInventoryIndexChanged);
        }
    }

    // Events Interface
    public void OnItemPickedUp(ItemType item, int index, int totalAmount)
    {
        Item itemInfo = ItemManager.Instance.GetItemByType(item);

        m_slots[index].Icon = itemInfo._UIIcon;
        m_slots[index].ItemCount = totalAmount;

        if(index == m_currentlySelectedSlot)
        {
            UpdateItemDisplayText(item);
        }
    }

    public void OnItemDropped(ItemType item, int index, int remainingAmount)
    {
        if(remainingAmount > 0)
        {
            m_slots[index].ItemCount = remainingAmount;
        }
        else
        {
            m_slots[index].Icon = null;
            m_slots[index].ItemCount = 0;
        }
    }

    public void OnInventoryIndexChanged(int index, ItemType itemAtIndex)
    {
        m_slots[m_currentlySelectedSlot].Selected = false;

        m_currentlySelectedSlot = index;
        m_slots[index].Selected = true;

        UpdateItemDisplayText(itemAtIndex);
    }

    public void HideDisplayText()
    {
        m_FadeItemDisplayText = true;
        m_FadeTimer = m_FadeSpeed;
    }

    private void UpdateItemDisplayText(ItemType item)
    {
        if(item != ItemType.MAX_ITEMS)
        {
            // Display the name of the item
            _selectedItemText.enabled = true;
            Color color = _selectedItemText.color;
            color.a = 1.0f;
            _selectedItemText.color = color;

            _selectedItemText.text = ItemManager.Instance.GetItemDisplayName(item);

            EventManager.Instance.AddTimedEvent(HideDisplayText, 1.0f);
        }
    }
}
