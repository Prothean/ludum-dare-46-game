﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyBar : MonoBehaviour
{
    // Constants
    public const float kFullEnergyAmount = 100.0f;

    // Exposed Variables
    [SerializeField] private float _energyDrainRate = 0.6333f;
    [SerializeField] private Slider m_slider = null;
    [SerializeField] private Text m_timeText = null;

    // Private Variables
    private float _energyAmount = 0;

    void Awake()
    {
        EventManager.Instance._OnEnergyLevelChangedEvents.Add(OnEnergyLevelChanged);
    }

    void Update()
    {
        if(_energyAmount <= 0.0f)
        {
            return;
        }

        // Drain the stored energy
        _energyAmount -= (_energyDrainRate * Time.deltaTime);

        if(_energyAmount <= 0.0f)
        {
            _energyAmount = 0.0f;
            EventManager.Instance.TriggerOnEnergyFullyDrainedEvent();
            Debug.Log("GAME OVER!");
        }

        float energyBarRatio = _energyAmount / kFullEnergyAmount;
        energyBarRatio = Mathf.Clamp(energyBarRatio, 0.0f, 1.0f);
        m_slider.value = energyBarRatio;

        float remainingSeconds = _energyAmount / _energyDrainRate;
        m_timeText.text = string.Format("{0:F1}s", remainingSeconds);
    }

    public void OnEnergyLevelChanged(float energyAmount)
    {
        _energyAmount = energyAmount;
    }
}
