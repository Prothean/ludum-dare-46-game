﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WinLossScreen : MonoBehaviour
{
    public Text ResultText;

    public void WinMessage()
    {
        ResultText.text = "YOU WIN";
    }

    public void LoseMessage()
    {
        ResultText.text = "TRY AGAIN";
    }

    public void RestartScene()
    {
        SceneManager.LoadScene(0);
    }

    public void QuitApplication()
    {
        Application.Quit();
    }
}
