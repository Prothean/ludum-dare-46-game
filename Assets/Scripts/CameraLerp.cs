﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraLerp : MonoBehaviour
{
    private Vector3 originalPos;
    private Quaternion originalRot;
    private bool beginLerp;
    private bool lerped;
    private float currDelta;

    public Vector3 TargetPos;
    public Vector3 TargetRot;
    public float lerpSpeed;
    public Brother_AI_Behaviour startRef;

    // Start is called before the first frame update
    void Start()
    {
        originalPos = transform.position;
        originalRot = transform.rotation;
        beginLerp = false;
        lerped = false;
        currDelta = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (beginLerp && !lerped)
        {
            transform.position = Vector3.Lerp(originalPos, TargetPos, currDelta);
            transform.rotation = Quaternion.Lerp(originalRot, Quaternion.Euler(TargetRot), currDelta);

            currDelta += Time.deltaTime * lerpSpeed;
            if (currDelta >= 1.0)
            {
                currDelta = 0.0f;
                transform.position = TargetPos;
                transform.eulerAngles = TargetRot;
                beginLerp = false;
                lerped = true;

                startRef.StartGameTimer();
            }
        }
    }

    public void ActivateCameraLerp()
    {
        beginLerp = true;
    }
}
