﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brother_AI_Behaviour : MonoBehaviour
{
    public string CharacterName;
    public List<string> RandomSittingReactionAnimations;
    public string TalkingAnimation;
    public float Delay;
    public TextWriter SpeechBubble;
    public string StartQuestDialogue;
    public string QIPLessThan33PercDialogue;
    public string QIPLessThan66PercDialogue;
    public string QIPLessThan100PercDialogue;
    public float TimeNeededToWinGame;
    public GameObject mainMenuRef;
    public GameObject hudRef;
    public Fan_AI_Behaviour Fan;
    public WinLossScreen winScreen;
    public GameObject pauseScreen;

    private Animator animator;
    private float delayTimer;
    private bool inIdle;
    private float winTimer;
    private bool gameStarted;

    private float firstTimeMarker;
    private float secondTimeMarker;
    private bool hitFirstMarker;
    private bool hitSecondMarker;

    private bool paused;

    // Start is called before the first frame update
    void Start()
    {
        delayTimer = 0.0f;
        inIdle = true;
        animator = GetComponent<Animator>();

        gameStarted = false;
        winTimer = 0.0f;

        firstTimeMarker = TimeNeededToWinGame * 0.33f;
        secondTimeMarker = TimeNeededToWinGame * 0.66f;

        string giverName = CharacterName + ":\n";

        StartQuestDialogue = giverName + StartQuestDialogue;
        QIPLessThan33PercDialogue = giverName + QIPLessThan33PercDialogue;
        QIPLessThan66PercDialogue = giverName + QIPLessThan66PercDialogue;
        QIPLessThan100PercDialogue = giverName + QIPLessThan100PercDialogue;

        SpeechBubble.setText(StartQuestDialogue);
        SpeechBubble.SetNextText(QIPLessThan33PercDialogue);
        SpeechBubble.gameObject.SetActive(false);

        paused = false;

        EventManager.Instance._OnEnergyFullyDrainedEvents.Add(LoseConditionMet);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.P))
        {
            paused = !paused;
            pauseScreen.SetActive(paused);

            if (paused)
            {
                Time.timeScale = 0.0f;
            }
            else
                Time.timeScale = 1.0f;
        }

        if (inIdle)
        {
            delayTimer += Time.deltaTime;
            if (delayTimer >= Delay)
            {
                inIdle = false;
                delayTimer = 0.0f;
                animator.Play(RandomSittingReactionAnimations[Random.Range(0, RandomSittingReactionAnimations.Count)]);
            }
        }
        else
        {
            delayTimer += Time.deltaTime;

            if (delayTimer >= 8.0f)
            {
                inIdle = true;
                delayTimer = 0.0f;
            }
        }

        if (gameStarted)
        {
            winTimer += Time.deltaTime;
        }

        if (winTimer >= firstTimeMarker && !hitFirstMarker)
        {
            hitFirstMarker = true;
            SpeechBubble.SetNextText(QIPLessThan66PercDialogue);
        }
        else if (winTimer >= secondTimeMarker && !hitSecondMarker)
        {
            hitSecondMarker = true;
            SpeechBubble.SetNextText(QIPLessThan100PercDialogue);
        }
        else if (winTimer >= TimeNeededToWinGame)
        {
            WinConditionMet();
        }


    }

    public void WinConditionMet()
    {
        winScreen.gameObject.SetActive(true);
        winScreen.WinMessage();
    }

    public void LoseConditionMet()
    {
        winScreen.gameObject.SetActive(true);
        winScreen.LoseMessage();
    }

    public void StartCameraLerp()
    {
        mainMenuRef.SetActive(false);

        Camera.main.GetComponent<CameraLerp>().ActivateCameraLerp();
    }

    public void StartGameTimer()
    {
        SpeechBubble.gameObject.SetActive(true);
        hudRef.SetActive(true);
        Fan.ShortCircuit();

        gameStarted = true;

        EventManager.Instance.TriggerOnGameStartEvent();
    }
}
