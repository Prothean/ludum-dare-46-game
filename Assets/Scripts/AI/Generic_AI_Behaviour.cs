﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generic_AI_Behaviour : MonoBehaviour
{
    public string CharacterName;
    public string SingleAnimation;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Animator>().Play(SingleAnimation);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
