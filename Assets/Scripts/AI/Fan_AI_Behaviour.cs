﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fan_AI_Behaviour : MonoBehaviour
{
    public float rotationSpeed;
    public float slowDownModifier;
    public GameObject particles;

    private bool beginSlowDown;
    private float currSpeed;
    private bool stoppedMoving;

    // Start is called before the first frame update
    void Start()
    {
        beginSlowDown = false;
        currSpeed = rotationSpeed;
        stoppedMoving = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!stoppedMoving)
        {
            if (beginSlowDown)
            {
                currSpeed -= slowDownModifier;

                transform.Rotate(Vector3.up * currSpeed * Time.deltaTime);

                if (currSpeed <= 0)
                    stoppedMoving = true;
            }
            else
            {
                transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
            } 
        }
    }

    public void ShortCircuit()
    {
        //Show some sort of particle effect
        beginSlowDown = true;

        particles.GetComponent<ParticleSystem>().Play();
    }
}
