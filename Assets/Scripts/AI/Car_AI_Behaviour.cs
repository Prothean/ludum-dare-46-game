﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Car_AI_Behaviour : MonoBehaviour
{
    public Waypoint FirstWaypoint;

    private List<RotateTire> tires;
    private Waypoint targetWaypoint;
    private NavMeshAgent navigationController;
    private bool disableCar;

    // Start is called before the first frame update
    void Start()
    {
        navigationController = GetComponent<NavMeshAgent>();
        tires = new List<RotateTire>(GetComponentsInChildren<RotateTire>());

        disableCar = !InitializeWaypoints();
    }

    // Update is called once per frame
    void Update()
    {
        if (!disableCar)
        {
            if (Vector3.Distance(transform.position, targetWaypoint.WaypointPosition) <= 5.0f)
            {
                targetWaypoint = targetWaypoint.NextWaypoint;

                navigationController.SetDestination(targetWaypoint.transform.position);
            }

            RotateTires();
        }
    }

    private void RotateTires()
    {
        foreach (RotateTire tire in tires)
        {
            tire.Rotate(navigationController.speed);
        }
    }

    private bool InitializeWaypoints()
    {
        if (FirstWaypoint != null)
        {
            targetWaypoint = FirstWaypoint;
            navigationController.SetDestination(targetWaypoint.WaypointPosition);

            return true;
        }

        return false;
    }
}
