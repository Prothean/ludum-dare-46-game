﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour
{
    public Waypoint NextWaypoint;
    public Vector3 WaypointPosition;
    public bool IsStoppoint;

    private void Awake()
    {
        WaypointPosition = transform.position;
    }
}
