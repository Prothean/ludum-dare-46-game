﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InteractableItem : MonoBehaviour
{
    // Enums
    public enum InteractableType
    {
        ItemPickup,
        DeliverMail,
        QuestStarted,
        QuestHandedIn,
        PickUpPhone,
        DropOffEnergy,
        None
    }

    // Exposed Variables
    // Audio clip to play when the player interacts with this container
    [SerializeField] protected AudioClip _interactWithSound = null;
    [SerializeField] protected float _interactSoundDelay = 0.0f;

    // Private Variables
    private Outline _outlineScript = null;
    private AudioSource _playerAudioSource = null;

    // Start is called before the first frame update
    protected void OnEnable()
    {
        // Try to get the outline script that's on this object
        _outlineScript = GetComponent<Outline>();
        if(_outlineScript == null)
        {
            // If there was no outline script, add one
            _outlineScript = gameObject.AddComponent<Outline>();

            // Setup the outline
            _outlineScript.OutlineMode = Outline.Mode.OutlineAll;
            _outlineScript.OutlineWidth = 8.0f;
            _outlineScript.OutlineColor = new Color(0.05f, 0.4f, 0.9f);
        }

        _outlineScript.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        PlayerController playerController = other.GetComponent<PlayerController>();
        if(playerController != null)
        {
            DisplayUIPrompt(true);

            playerController.SetItemInRange(this);
        }

        _outlineScript.enabled = true;
    }

    //When the Primitive exits the collision, it will change Color
    private void OnTriggerExit(Collider other)
    {
        PlayerController playerController = other.GetComponent<PlayerController>();
        if(playerController != null)
        {
            DisplayUIPrompt(false);

            playerController.SetItemInRange(null);
        }

        _outlineScript.enabled = false;
    }

    // Public Interface
    public void InteractWith(PlayerController player, Animator playerAnimator, AudioSource playerAudioSource = null)
    {
        _playerAudioSource = playerAudioSource;

        // Play the interact sound, if there is one
        if(_interactWithSound != null && playerAudioSource != null)
        {   
            if(_interactSoundDelay > 0.0f)
            {
                EventManager.Instance.AddTimedEvent(PlayInteractWithSound, _interactSoundDelay);
            }
            else
            {
                PlayInteractWithSound();
            }
        }

        Interact(player, playerAnimator);
    }

    // Protected Interface
    protected void ShowModelOutline(bool show)
    {
        _outlineScript.enabled = show;
    }

    // Virtual Interface
    abstract public InteractableType Interact(PlayerController player, Animator playerAnimator);

    virtual protected void DisplayUIPrompt(bool display)
    {
        if (display == true)
        {
            EventManager.Instance.TriggerOnInteractMessageEvent("To Interact");
        }
        else
        {
            EventManager.Instance.TriggerOnInteractMessageEvent("");
        }
    }

    // Events Interface
    public void PlayInteractWithSound()
    {
        _playerAudioSource.PlayOneShot(_interactWithSound);
    }
}
