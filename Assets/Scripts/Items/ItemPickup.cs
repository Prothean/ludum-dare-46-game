﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : InteractableItem
{
    // Exposed Variables
    [SerializeField] ItemType _itemType;
    //! Delay time for when deleting the item after the player picks it up (To match the pick up animation)
    [SerializeField] float _itemDeleteDelayTime = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
        _interactWithSound = Resources.Load<AudioClip>("Sounds/Items/ItemPickup");
        _interactSoundDelay = 0.3f;
    }

    public ItemType GetItemType()
    {
        return _itemType;
    }

    override public InteractableType Interact(PlayerController player, Animator playerAnimator)
    {
        if(player.PickUpItem(_itemType) == false)
        {
            // Player couldn't pick up the item for whatever reason
            return InteractableType.None;
        }

        // Turn of collision
        Collider collider = GetComponent<Collider>();
        collider.enabled = false;

        // Turn of UI
        DisplayUIPrompt(false);

        // Player can no longer interact with it
        player.SetItemInRange(null);

        // Delete the object
        Destroy(this.gameObject, _itemDeleteDelayTime);

        return InteractableType.ItemPickup;
    }

    override protected void DisplayUIPrompt(bool display)
    {
        if(display == true)
        {
            EventManager.Instance.TriggerOnInteractMessageEvent("To Pickup");
        }
        else
        {
            EventManager.Instance.TriggerOnInteractMessageEvent("");
        }
    }
}
