﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyDropoff : InteractableItem
{
    // Exposed Variables
    [SerializeField] private float _energyAmount = 0;
    [SerializeField] protected AudioClip _successSound = null;
    [SerializeField] protected AudioClip _errorSound = null;

    // Unity Interface
    void Start()
    {
        EventManager.Instance._OnGameStartEvents.Add(OnGameStart);

        // Trigger Events
        EventManager.Instance.TriggerOnEnergyLevelChangedEvent(_energyAmount);
    }

    // Interactable Item Interface
    override public InteractableType Interact(PlayerController player, Animator playerAnimator)
    {
        // Check if the player has energy to drop off
        Inventory playerInventory = player.GetInventory();
        ItemType selectedItem = playerInventory.GetCurrentlySelectedItem();
        if(selectedItem == ItemType.MAX_ITEMS)
        {
            // No valid selected item

            // Total hax
            _interactWithSound = _errorSound;
            PlayInteractWithSound();
            _interactWithSound = null;

            return InteractableType.None;
        }

        float energyAmount = ItemManager.Instance.GetItemEnergyAmount(selectedItem);
        if(energyAmount <= 0.0f)
        {
            // Item has no energy

            // Total hax
            _interactWithSound = _errorSound;
            PlayInteractWithSound();
            _interactWithSound = null;

            return InteractableType.None;
        }

        // Remove the item
        playerInventory.RemoveItemAtCurrentSelectedSlot();

        _energyAmount += energyAmount;

        // Total hax
        _interactWithSound = _successSound;
        PlayInteractWithSound();
        _interactWithSound = null;

        // Trigger Events
        EventManager.Instance.TriggerOnEnergyLevelChangedEvent(_energyAmount);

        // Handle the animations
        playerAnimator.Play("PuttingDown");

        return InteractableType.DropOffEnergy;
    }

    override protected void DisplayUIPrompt(bool display)
    {
        if(display == true)
        {
            EventManager.Instance.TriggerOnInteractMessageEvent("To Deposit Energy");
        }
        else
        {
            EventManager.Instance.TriggerOnInteractMessageEvent("");
        }
    }

    // Events Interface
    public void OnGameStart()
    {
        // Trigger Events
        EventManager.Instance.TriggerOnEnergyLevelChangedEvent(_energyAmount);
    }
}
