﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleRotate : MonoBehaviour
{
    // Enums
    public enum RotateAxis
    {
        X,
        Y,
        Z
    }

    // Exposed Variables
    [SerializeField] float _rotateSpeed = 180.0f;
    [SerializeField] RotateAxis _axisToRotateAround = RotateAxis.Y;

    // Private Variables
    private Vector3 _rotation;

    // Start is called before the first frame update
    void Start()
    {
        switch(_axisToRotateAround)
        {
            case RotateAxis.X:
                _rotation = new Vector3(1.0f, 0.0f, 0.0f);
                break;
            case RotateAxis.Y:
                _rotation = new Vector3(0.0f, 1.0f, 0.0f);
                break;
            case RotateAxis.Z:
                _rotation = new Vector3(0.0f, 0.0f, 1.0f);
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(_rotation, _rotateSpeed * Time.deltaTime);
    }
}
