﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhoneBooth : InteractableItem
{
    // Exposed Variables
    [SerializeField] Collider _triggerCollider = null;
    [SerializeField] AudioSource _ringingAudioSource = null;

    // Unity Interface
    void Start()
    {
        if(_ringingAudioSource == null)
        {
            // Try to get the Audio source
            _ringingAudioSource = GetComponent<AudioSource>();
            if(_ringingAudioSource == null)
            {
                Debug.LogError("PhoneBooth: No AudioSource defined and unable to find an AudioSource on the object!");
            }
        }
    }

    new protected void OnEnable()
    {
        // TESTING-ONLY
        Debug.Log("PhoneBooth::OnEnable called!");
        // END-TESTING
        base.OnEnable();
        _triggerCollider.enabled = true;

        if(_ringingAudioSource != null)
        {
            // TESTING-ONLY
            Debug.Log("PhoneBooth: Playing Audio");
            // END-TESTING
            _ringingAudioSource.Play();
        }
    }

    protected void OnDisable()
    {
        _triggerCollider.enabled = false;

        // Turn off the ringing sound
        if(_ringingAudioSource != null)
        {
            _ringingAudioSource.Stop();
        }
    }

    // InteractableItem Interface
    override public InteractableType Interact(PlayerController player, Animator playerAnimator)
    {
        // Turn of collision
        Collider collider = GetComponent<Collider>();
        collider.enabled = false;
        ShowModelOutline(false);

        // Turn off the ringing sound
        if(_ringingAudioSource != null)
        {
            _ringingAudioSource.Stop();
        }

        // Turn of UI
        DisplayUIPrompt(false);

        // Player can no longer interact with it
        player.SetItemInRange(null);

        // Play the animation
        playerAnimator.Play("PushButton");

        // Trigger event
        EventManager.Instance.TriggerOnPickUpPhoneEvent();

        return InteractableType.PickUpPhone;
    }

    override protected void DisplayUIPrompt(bool display)
    {
        if(display == true)
        {
            EventManager.Instance.TriggerOnInteractMessageEvent("To Answer");
        }
        else
        {
            EventManager.Instance.TriggerOnInteractMessageEvent("");
        }
    }
}
