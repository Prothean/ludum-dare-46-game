﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// HOW TO ADD ITEMS:
// 1. Add your item entry to the `ItemType` enum below
// 2. Create a prefab of your item, and attach some variant of the `InteractableItem` component on to it (more than likely you'll want the `ItemPickup` component)
// 3. In the main scene (or whatever scene) find the GameObject that represents the `ItemManager`
// 3.1. In the properties of the `ItemManager` GameObject, create a new entry in the `_items` list and fill in all the information about your item
// 4. Profit baby

public enum ItemType
{
    WateringCan = 0,
    SmallBattery,
    MediumBattery,
    LargeBattery,
    Corn,
    HolyTomato,
    Toolbox,
    LostMovingBox,
    PizzaBox,
    Soda,
    EnergyDrink,
    MAX_ITEMS
}

[System.Serializable]
public class Item
{
    //! Type fo the item, used for lookups / storage
    public ItemType _type;
    //! Whether or the item can be stacked when stored in the inventory
    public bool _allowItemStacking;
    //! Display name of the Item
    public string _displayName;
    //! Icon that is displayed in the UI
    public Sprite _UIIcon = null;
    //! Amount of energy this item provides (not all items need to provide energy, so you can just leave it as 0 if that's the case)
    public float _energyAmount = 0;
    //! In-game representation of the item
    public GameObject _prefab;
}

public class ItemManager : MonoBehaviour
{
    // Singleton
    public static ItemManager Instance { get; private set; }

    // Exposed Variables
    [SerializeField] List<Item> _items = new List<Item>();

    // Private Variables
    private Dictionary<ItemType, Item> _itemMap = new Dictionary<ItemType, Item>();

    // Unity Interface
    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    void Start()
    {
        // Verify that we've defined all of the items
        if(Instance._items.Count < (int)ItemType.MAX_ITEMS)
        {
            Debug.LogError("ItemManager._items doesn't have all of the items defined in it, did we forget to add something?");
        }

        // Convert the list of items to a dictionary, to allow easy look ups
        foreach(Item item in Instance._items)
        {
            Instance._itemMap.Add(item._type, item);
        }

        // Clear the list, only the ItemMap should be used from now on
        Instance._items.Clear();
    }

    // Public Interface
    public Item GetItemByType(ItemType type)
    {
        if(Instance._itemMap.ContainsKey(type))
        {
            return Instance._itemMap[type];
        }
        else
        {
            Debug.Log("Not item found with the type: " + type.ToString());
            return null;
        }
    }

    public string GetItemDisplayName(ItemType type)
    {
        if(Instance._itemMap.ContainsKey(type))
        {
            return Instance._itemMap[type]._displayName;
        }
        else
        {
            Debug.Log("Not item found with the type: " + type.ToString());
            return "Unknown";
        }
    }

    public float GetItemEnergyAmount(ItemType type)
    {
        if(Instance._itemMap.ContainsKey(type))
        {
            return Instance._itemMap[type]._energyAmount;
        }
        else
        {
            Debug.Log("Not item found with the type: " + type.ToString());
            return 0;
        }
    }

    public bool CanItemStack(ItemType type)
    {
        if(Instance._itemMap.ContainsKey(type))
        {
            return Instance._itemMap[type]._allowItemStacking;
        }
        else
        {
            Debug.Log("Not item found with the type: " + type.ToString());
            return false;
        }
    }

    public void SpawnItemAtLocation(ItemType type, Vector3 position)
    {
        if(Instance._itemMap.ContainsKey(type))
        {
            // Instantiate at the given position and zero rotation.
            Instantiate(Instance._itemMap[type]._prefab, position, Quaternion.identity);
        }
        else
        {
            Debug.Log("Not item found with the type: " + type.ToString());
        }
    }

    public void SpawnItemAtLocation(ItemType type, Vector3 position, Vector3 rotation)
    {
        if(Instance._itemMap.ContainsKey(type))
        {
            // Instantiate at the given position and rotation.
            Instantiate(Instance._itemMap[type]._prefab, position, Quaternion.Euler(rotation));
        }
        else
        {
            Debug.Log("Not item found with the type: " + type.ToString());
        }
    }
}
