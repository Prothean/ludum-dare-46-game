﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemContainer : InteractableItem
{
    // Enums
    public enum AnimationType
    {
        None,
        PushButtons
    }

    // Exposed Variables
    //! Type of Animation for the player to do when interacting
    [SerializeField] AnimationType _playerAnimationType = AnimationType.None;
    //! Delay for when the items spawn
    [SerializeField] float _itemSpawnDelay = 0.0f;
    //! Initial forward distance where the items will start to spawn
    [SerializeField] float _itemInitialSpace = 1.0f;
    //! Distance between spawned items
    [SerializeField] float _itemSpacing = 0.9f;
    //! List of items that this container has
    [SerializeField] List<ItemType> _items = new List<ItemType>();
    [SerializeField] Collider _interactTrigger = null;

    // Unity Interface
    void Start()
    {
        // *Note*: It's still best to manually set up what collider is the interact trigger
        if(_interactTrigger == null)
        {
            // Query for the trigger
            Collider[] colliders = GetComponents<Collider>();
            foreach(Collider collider in colliders)
            {
                if(collider.isTrigger == true)
                {
                    // Assume that the first trigger collider is the interact trigger
                    _interactTrigger = collider;
                    break;
                }
            }
        }
    }

    // Interactable Item Interface
    override public InteractableType Interact(PlayerController player, Animator playerAnimator)
    {
        // Turn of the interact trigger, so that the container can't be opened again
        _interactTrigger.enabled = false;
        ShowModelOutline(false);

        // Turn of UI
        DisplayUIPrompt(false);

        // Player can no longer interact with it
        player.SetItemInRange(null);

        // Handle the animations
        PlayAnimation(playerAnimator);

        // Spawn the items in front of the container
        if(_itemSpawnDelay > 0.0f)
        {
            EventManager.Instance.AddTimedEvent(SpawnItems, _itemSpawnDelay);
        }
        else
        {
            SpawnItems();
        }

        return InteractableType.ItemPickup;
    }

    // Private Interface
    private void PlayAnimation(Animator playerAnimator)
    {
        switch(_playerAnimationType)
        {
            case AnimationType.PushButtons:
                playerAnimator.Play("PushButton");
                break;
        }
    }

    // Event Interface
    public void SpawnItems()
    {
        Vector3 itemPosition = transform.position + (transform.forward * _itemInitialSpace) + (transform.right * -_itemSpacing);
        for(int i = 0; i < _items.Count; ++i)
        {
            // Every 3 items, move forward
            int modValue = i % 3;
            if(i != 0 && modValue == 0)
            {
                itemPosition += (transform.forward * _itemInitialSpace) + (transform.right * -_itemSpacing * 3);
            }

            ItemManager.Instance.SpawnItemAtLocation(_items[i], itemPosition);
            itemPosition += transform.right * _itemSpacing;
        }
    }
}
