﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickablePlant : InteractableItem
{
    // Exposed Variables
    [SerializeField] private ItemType _itemType;
    [SerializeField] private int _itemCount = 1;
    //! Delay time for when deleting the item after the player picks it up (To match the pick up animation)
    [SerializeField] private float _itemDeleteDelayTime = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Interactable Item Interface
    override public InteractableType Interact(PlayerController player, Animator playerAnimator)
    {
        // Try have the player pick the plant
        if(player.GetInventory().AddItem(_itemType, _itemCount) == Inventory.kInventoryFull)
        {
            // Player didn't have any room for the items
            return InteractableType.None;
        }

        // TODO: Make the player face the plant?

        // Play the character's animation
        playerAnimator.Play("PickingFruit");

        // Turn off collision
        Collider collider = GetComponent<Collider>();
        collider.enabled = false;

        // Turn off UI
        DisplayUIPrompt(false);

        // Player can no longer interact with it
        player.SetItemInRange(null);

        // Delete the object
        if(_itemDeleteDelayTime > 0.0f)
        {
            Destroy(this.gameObject, _itemDeleteDelayTime);
        }

        return InteractableType.ItemPickup;
    }
}
