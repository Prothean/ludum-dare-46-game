﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mailbox : InteractableItem
{
    // Exposed Variables
    [SerializeField] GameObject _flag = null;
    [SerializeField] Collider _triggerCollider = null;

    // Private Variables
    private bool _mailDelivered = false;

    // Unity Interface
    protected void OnEnable()
    {
        base.OnEnable();

        _triggerCollider.enabled = true;
        _flag.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
    }

    protected void OnDisable()
    {
        _triggerCollider.enabled = false;
        _flag.transform.localRotation = Quaternion.Euler(-90.0f, 0.0f, 0.0f);
    }

    // InteractableItem Interface
    override public InteractableType Interact(PlayerController player, Animator playerAnimator)
    {
        // Turn of collision
        Collider collider = GetComponent<Collider>();
        collider.enabled = false;
        ShowModelOutline(false);

        // Turn of UI
        DisplayUIPrompt(false);

        // Player can no longer interact with it
        player.SetItemInRange(null);

        // Trigger event
        EventManager.Instance.TriggerOnDeliverMailEvent();

        // Put the flag down
        _flag.transform.localRotation = Quaternion.Euler(-90.0f, 0.0f, 0.0f);

        return InteractableType.DeliverMail;
    }

    override protected void DisplayUIPrompt(bool display)
    {
        if(display == true)
        {
            EventManager.Instance.TriggerOnInteractMessageEvent("To Drop Off Mail");
        }
        else
        {
            EventManager.Instance.TriggerOnInteractMessageEvent("");
        }
    }
}
