﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    // *********** EVENT *********** //
    public delegate void TimerEndEvent();
    public TimerEndEvent OnTimerEnd;

    private float m_currentTime = 0.0f;
    public float CurrentTime { get; private set; }

    private float m_targetTime = 0.0f;
    public float TargetTime { get; set; }

    private bool m_isPaused = true;
    public bool IsPaused { get; private set; }

    private bool m_hasEventBeenInvoked = false;



    public void Play()
    {
        m_isPaused = false;
    }

    public void Pause()
    {
        m_isPaused = true;
    }

    public void Reset()
    {
        m_currentTime = 0.0f;
        m_hasEventBeenInvoked = false;
    }

    private void Update()
    {
        if (m_isPaused)
            return;

        CountdownTimer();
        CheckTimer();
    }

    private void CountdownTimer()
    {
        m_currentTime += Time.deltaTime;
    }

    private void CheckTimer()
    {
        if (m_hasEventBeenInvoked)
            return;

        if (CurrentTime < m_targetTime)
            return;

        OnTimerEnd?.Invoke();
        m_hasEventBeenInvoked = true;
    }
}
