﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BuildingLevel
{
    // Public Variables
    public List<GameObject> _objects = new List<GameObject>(); // This can be a high level game object, that contains multiple children

    // Private Variables
    private List<MeshRenderer> _meshRenderers = new List<MeshRenderer>();
    private List<BoxCollider> _colliders = new List<BoxCollider>();

    public void Initialize()
    {
        // Get all the MeshRenderers for each GameObject (and all of it's children)
        foreach(GameObject obj in _objects)
        {
            MeshRenderer[] meshRenderers = obj.GetComponents<MeshRenderer>();
            _meshRenderers.AddRange(meshRenderers);

            meshRenderers = obj.GetComponentsInChildren<MeshRenderer>();
            _meshRenderers.AddRange(meshRenderers);

            BoxCollider[] colliders = obj.GetComponents<BoxCollider>();
            _colliders.AddRange(colliders);

            colliders = obj.GetComponentsInChildren<BoxCollider>();
            _colliders.AddRange(colliders);
        }
    }

    public void Show(bool show)
    {
        foreach(MeshRenderer renderer in _meshRenderers)
        {
            renderer.enabled = show;
        }

        foreach(BoxCollider collider in _colliders)
        {
            collider.enabled = show;
        }
    }
}

public class EnterableBuilding : MonoBehaviour
{
    // Exposed Variables
    [SerializeField] private List<BuildingLevel> _levels = new List<BuildingLevel>();

    // Private Variables
    private int _activeLevel = -1; // `-1` means that everything is shown (i.e. if the player is outside the building)

    // Unity Interface
    void Start()
    {
        foreach(BuildingLevel level in _levels)
        {
            level.Initialize();
        }
    }

    // Public Interface
    public int GetActiveLevel()
    {
        return _activeLevel;
    }

    public void SetActiveLevel(int level)
    {
        if(level >= _levels.Count)
        {
            Debug.LogError("Wanted Active Level is out of bounds!");
            return;
        }

        if(_activeLevel > -1)
        {
            // Valid previous active level
            if(level > _activeLevel)
            {
                // New active level is higher than the previous, just turn on any levels below the wanted level
                for(int i = _activeLevel; i <= level; ++i)
                {
                    ShowLevelObjects(i, true);
                }
            }
            else
            {
                // New active level is below the previous active level, so we just need to turn off all levels up to the previous level
                for(int i = level + 1; i <= _activeLevel; ++i)
                {
                    ShowLevelObjects(i, false);
                }
            }
        }
        else
        {
            // No valid previous active level, hide all levels above the wanted
            for(int i = level + 1; i < _levels.Count; ++i)
            {
                ShowLevelObjects(i, false);
            }
        }

        _activeLevel = level;
    }

    public void ShowAll()
    {
        // Show all the levels (meant for when a player exits a building
        for(int i = _activeLevel + 1; i < _levels.Count; ++i)
        {
            ShowLevelObjects(i, true);
        }

        _activeLevel = -1;
    }

    // Private Interface
    private void ShowLevelObjects(int level, bool show)
    {
        _levels[level].Show(show);
    }
}
