﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterableBuildingTrigger : MonoBehaviour
{
    // Enums
    private enum State
    {
        Enter,
        Exit
    }

    // Exposed Variables
    [SerializeField] int _correspondingLevel = 0;
    [SerializeField] bool _leadsOutside = true;
    [SerializeField] EnterableBuilding _building = null;
    [SerializeField] List<EnterableBuildingTrigger> _relatedTriggers = new List<EnterableBuildingTrigger>();

    // Private Variables
    private State _currentState;

    // Start is called before the first frame update
    void Start()
    {
        if(_building == null)
        {
            Debug.LogError("No building defined for EnterableBuildingTrigger:", this.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        switch(_currentState)
        {
            case State.Enter:
                _building.SetActiveLevel(_correspondingLevel);
                _currentState = State.Exit;

                break;
            case State.Exit:
                if(_leadsOutside == true || _correspondingLevel == 0)
                {
                    _building.ShowAll();
                   
                }
                else
                {
                    _building.SetActiveLevel(_correspondingLevel - 1);
                }

                _currentState = State.Enter;
                break;
        }

        foreach(EnterableBuildingTrigger trigger in _relatedTriggers)
        {
            trigger._currentState = _currentState;
        }
    }
}
