﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundAudioManager : MonoBehaviour
{
    //Used to quickly indicate that something has failed in the ambient audio system
    public AudioClip ErrorAmbience;

    public AudioClip OutsideAmbience;
    public AudioClip ChurchAmbience;
    public AudioClip HomeAmbience;

    private static AudioSource source;
    private static string currentLocationStatic;

    private static AudioClip errorAmbienceStatic;
    private static AudioClip outsideAmbienceStatic;
    private static AudioClip churchAmbienceStatic;
    private static AudioClip homeAmbienceStatic;

    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();

        errorAmbienceStatic = ErrorAmbience;

        outsideAmbienceStatic = OutsideAmbience;
        churchAmbienceStatic = ChurchAmbience;
        homeAmbienceStatic = HomeAmbience;

        ChangeBackgroundAmbience("Outside");
    }

    public static void ChangeBackgroundAmbience(string locationName)
    {
        if (locationName != currentLocationStatic)
        {
            AudioClip newAmbience = errorAmbienceStatic;

            switch (locationName)
            {
                case "Home":
                    newAmbience = homeAmbienceStatic;
                    break;
                case "Outside":
                    newAmbience = outsideAmbienceStatic;
                    break;
                case "Church":
                    newAmbience = churchAmbienceStatic;
                    break;
            }

            currentLocationStatic = locationName;
            source.clip = newAmbience;
            source.Play();
        }
    }
}
