﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utilities
{
    // --- Mouse Defines
    public const int kLeftMouseButton = 0;
    public const int kRightMouseButton = 1;
    public const int kMiddleMouseButton = 2;
    public const string kMouseXAxis = "Mouse X";
    public const string kMouseYAxis = "Mouse Y";
}
