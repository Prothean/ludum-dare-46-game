﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Exposed Variables
    [SerializeField] private Animator _modelAnimator = null;
    [SerializeField] private float _walkSpeed = 3.0f;
    [SerializeField] private float _runSpeed = 5.0f;
    [SerializeField] private float _rotateSpeed = 60.0f;
    [SerializeField] private float _cameraRotateSpeed = 360.0f;
    [SerializeField] private Transform _cameraParentTransform = null;
    [SerializeField] private Transform _cameraPitchTransform = null;
    [SerializeField] private bool _restrictMovementAtStart = true;

    // Private Variables
    private bool _cameraRotated = false;
    private Quaternion _originalCameraHorizontalRotation;
    private Quaternion _originalCameraVerticalRotation;
    private InteractableItem _itemInRange = null;
    private Inventory _inventory = new Inventory();
    private AudioSource _itemInteractAudioSource = null;

    // Start is called before the first frame update
    void Start()
    {
        if(_cameraParentTransform != null && _cameraPitchTransform != null)
        {
            _originalCameraHorizontalRotation = _cameraParentTransform.localRotation;
            _originalCameraVerticalRotation = _cameraPitchTransform.localRotation;
        }
        else
        {
            Debug.Assert(false, "Camera transforms are undefined!", this);
        }

        // Try to get the audio source for interacting with items
        _itemInteractAudioSource = GetComponent<AudioSource>();

        EventManager.Instance._OnGameStartEvents.Add(OnGameStart);
    }

    void OnDestroy()
    {
        if(EventManager.Instance != null)
        {
            EventManager.Instance._OnGameStartEvents.Remove(OnGameStart);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(_restrictMovementAtStart == false)
        {
            ProcessInput();
            ProcessInteract();
            ProcessInventoryInput();
        }
    }

    // Public Interface
    public Inventory GetInventory()
    {
        return _inventory;
    }

    public void SetItemInRange(InteractableItem item)
    {
        _itemInRange = item;
    }

    public bool PickUpItem(ItemType item)
    {
        if(_inventory.AddItem(item, 1) == Inventory.kInventoryFull)
        {
            // Wasn't able to pick up the item for some reason
            return false;
        }

        _modelAnimator.Play("PickingUp");

        return true;
    }

    // Private Interface
    private void ProcessInput()
    {
        float forwardMovement = 0.0f;

        // Capture any input
        float speed = _walkSpeed;
        if(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            speed = _runSpeed;
        }

        if(Input.GetKey(KeyCode.W))
        {
            forwardMovement = speed * Time.deltaTime;
        }
        else if(Input.GetKey(KeyCode.S))
        {
            forwardMovement = -speed * Time.deltaTime;
        }

        // Move the player
        if(forwardMovement != 0.0f)
        {
            _modelAnimator.SetFloat("Speed", speed);
            transform.position += (transform.forward * forwardMovement);
        }
        else
        {
            _modelAnimator.SetFloat("Speed", 0.0f);
        }

        // Rotate the player
        if(Input.GetKey(KeyCode.A))
        {
            transform.Rotate(0.0f, -_rotateSpeed * Time.deltaTime, 0.0f);
        }
        else if(Input.GetKey(KeyCode.D))
        {
            transform.Rotate(0.0f, _rotateSpeed * Time.deltaTime, 0.0f);
        }

        // Rotate the camera
        if(Input.GetMouseButton(Utilities.kRightMouseButton))
        {
            _cameraRotated = true;

            float horizontalRotation = Input.GetAxis(Utilities.kMouseXAxis) * _cameraRotateSpeed * Time.deltaTime;
            _cameraParentTransform.Rotate(0.0f, horizontalRotation, 0.0f);

            float verticalRotation = _cameraPitchTransform.rotation.eulerAngles.x + Input.GetAxis(Utilities.kMouseYAxis) * _cameraRotateSpeed * Time.deltaTime;
            verticalRotation = Mathf.Clamp(verticalRotation, 0, 80);
            Quaternion newRotation = _cameraPitchTransform.localRotation;
            newRotation.eulerAngles = new Vector3(verticalRotation, 0.0f, 0.0f);
            _cameraPitchTransform.localRotation = newRotation;
        }
        else if(_cameraRotated == true)
        {
            _cameraRotated = false;

            // Reset the camera orientation
            _cameraParentTransform.localRotation = _originalCameraHorizontalRotation;
            _cameraPitchTransform.localRotation = _originalCameraVerticalRotation;
        }
    }

    private void ProcessInteract()
    {
        if(_itemInRange != null && Input.GetKeyDown(KeyCode.E))
        {
            _itemInRange.InteractWith(this, _modelAnimator, _itemInteractAudioSource);
        }
    }

    private void ProcessInventoryInput()
    {
        // Change what item the player is currently selecting
        float value = Input.GetAxis("Mouse ScrollWheel");
        if(value < 0f)
        {
            _inventory.NextSlotIndex();
        }
        else if(value > 0f)
        {
            _inventory.PrevSlotIndex();
        }

        // Drop the currently selected item
        if(Input.GetKey(KeyCode.F))
        {
            ItemType droppedType = _inventory.RemoveItemAtCurrentSelectedSlot();

            if(droppedType != ItemType.MAX_ITEMS)
            {
                ItemManager.Instance.SpawnItemAtLocation(droppedType, transform.position);
            }
        }
    }

    // Events Interface
    public void OnGameStart()
    {
        // Allow the player to move again
        _restrictMovementAtStart = false;
    }
}
