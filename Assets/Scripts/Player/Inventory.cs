﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory
{
    private class InventorySlot
    {
        public bool _allowStacking = true;
        public ItemType _type;
        public int _count = 0;
    }

    // Constants
    public const int kInventorySlots = 8;
    public const int kInventoryFull = -1;

    // Properties
    public int CurrentSelectedSlot
    {
        get
        {
            return _currentlySelectedItem;
        }
    }

    // Private Variables
    private InventorySlot[] _inventorySlots = new InventorySlot[kInventorySlots];
    private int _currentlySelectedItem = 0;

    // Public Interface
    public void NextSlotIndex()
    {
        ++_currentlySelectedItem;
        if(_currentlySelectedItem >= kInventorySlots)
        {
            _currentlySelectedItem = 0;
        }

        OnSetSelectedIndex();
    }

    public void PrevSlotIndex()
    {
        --_currentlySelectedItem;
        if(_currentlySelectedItem < 0)
        {
            _currentlySelectedItem = kInventorySlots - 1;
        }

        OnSetSelectedIndex();
    }

    public ItemType GetCurrentlySelectedItem()
    {
        if(_inventorySlots[_currentlySelectedItem] != null)
        {
            return _inventorySlots[_currentlySelectedItem]._type;
        }

        return ItemType.MAX_ITEMS;
    }

    public ItemType RemoveItemAtCurrentSelectedSlot()
    {
        if(_inventorySlots[_currentlySelectedItem] != null)
        {
            ItemType type = _inventorySlots[_currentlySelectedItem]._type;
            _inventorySlots[_currentlySelectedItem]._count -= 1;

            if(_inventorySlots[_currentlySelectedItem]._count <= 0)
            {
                // No more of that item left, clear out the slot
                _inventorySlots[_currentlySelectedItem] = null;
            }

            // Let any listeners know that the player has picked up a new item
            EventManager.Instance.TriggerOnItemDropEvent(type, _currentlySelectedItem, 0);

            return type;
        }

        return ItemType.MAX_ITEMS;
    }

    public int AddItem(ItemType item, int amount)
    {
        bool canStack = ItemManager.Instance.CanItemStack(item);
        if(canStack == true)
        {
            // See if this item type is already stored
            for(int i = 0; i < kInventorySlots; ++i)
            {
                if(_inventorySlots[i] != null && _inventorySlots[i]._type == item)
                {
                    _inventorySlots[i]._count += amount;

                    // Let any listeners know that the player has picked up a new item
                    EventManager.Instance.TriggerOnItemPickupEvent(item, i, _inventorySlots[i]._count);

                    // Return the index where the item was added to
                    return i;
                }
            }
        }

        // Find the next available slot
        for(int i = 0; i < kInventorySlots; ++i)
        {
            if(_inventorySlots[i] == null)
            {
                _inventorySlots[i] = new InventorySlot();
                _inventorySlots[i]._allowStacking = canStack;
                _inventorySlots[i]._type = item;
                _inventorySlots[i]._count = amount;

                // Let any listeners know that the player has picked up a new item
                EventManager.Instance.TriggerOnItemPickupEvent(item, i, amount);

                return i;
            }
        }

        // No open slots for the item
        return kInventoryFull;
    }

    public void RemoveItemByType(ItemType item, int amount)
    {
        int itemsRemoved = 0;

        for(int i = 0; i < kInventorySlots; ++i)
        {
            if(_inventorySlots[i] != null && _inventorySlots[i]._type == item)
            {
                if(_inventorySlots[i]._count <= amount)
                {
                    itemsRemoved += _inventorySlots[i]._count;
                    _inventorySlots[i]._count -= amount;

                    int newCount = _inventorySlots[i]._count;
                    if(newCount == 0)
                    {
                        // No more of that item left, clear out the slot
                        _inventorySlots[i] = null;
                    }

                    // Let any listeners know that the player has picked up a new item
                    EventManager.Instance.TriggerOnItemDropEvent(item, i, newCount);
                }

                if(itemsRemoved >= amount)
                {
                    return;
                }
            }
        }
    }

    public bool ContainsItem(ItemType item, int amount = 1)
    {
        int found = 0;
        for(int i = 0; i < kInventorySlots; ++i)
        {
            if(_inventorySlots[i] != null && _inventorySlots[i]._type == item)
            {
                found += _inventorySlots[i]._count;

                if(found >= amount)
                {
                    return true;
                }
            }
        }

        return false;
    }

    // Private Interface
    private void OnSetSelectedIndex()
    {
        ItemType type = ItemType.MAX_ITEMS;
        if(_inventorySlots[_currentlySelectedItem] != null)
        {
            type = _inventorySlots[_currentlySelectedItem]._type;
        }

        EventManager.Instance.TriggerOnInventoryIndexChangedEvent(_currentlySelectedItem, type);
    }
}
