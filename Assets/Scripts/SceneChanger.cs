﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Animator))]
public class SceneChanger : MonoBehaviour
{
    private Animator m_animator = null;
    private int m_levelToLoad = 0;

    public static SceneChanger instance { get; private set; }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            m_animator = GetComponent<Animator>();
            DontDestroyOnLoad(instance.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneChanged;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneChanged;
    }

    public void FadeToNextLevel()
    {
        int nextSceneIndex = SceneManager.GetActiveScene().buildIndex + 1;
        if (!isValidSceneIndex(nextSceneIndex))
        {
           // Debug.LogError(nextSceneIndex + " is not a valid scene index"); Sorry Doug. It's for the JAAAAAM!
            return;
        }

        instance.m_animator.SetTrigger("StartFadeToBlack");
        instance.m_levelToLoad = nextSceneIndex;
    }

    public void FadeToPreviousLevel()
    {
        int previousSceneIndex = SceneManager.GetActiveScene().buildIndex - 1;
        if (!isValidSceneIndex(previousSceneIndex))
        {
            Debug.LogError(previousSceneIndex + " is not a valid scene index");
            return;
        }

        instance.m_animator.SetTrigger("StartFadeToBlack");
        instance.m_levelToLoad = previousSceneIndex;
    }

    public void ReloadCurrentScene()
    {
        instance.m_animator.SetTrigger("StartFadeToBlack");
        instance.m_levelToLoad = SceneManager.GetActiveScene().buildIndex;
    }

    // *********************** CALLBACKS *********************** //
    public void OnFadeToBlackComplete()
    {
       // SceneManager.LoadScene(m_levelToLoad); Sorry Doug. BUT JAAAM
    }

    public void OnFadeToClearComplete()
    {
        
    }

    private void OnSceneChanged(Scene _scene, LoadSceneMode _mode)
    {
        instance.m_animator.Play("SceneFadeToClear");
    }
    // ********************************************************* //

    private bool isValidSceneIndex(int _sceneIndex)
    {
        return (_sceneIndex >= 0 && _sceneIndex <= SceneManager.sceneCount);
    }
}
