﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Enums
public enum QuestType
{
    FlyerDelivery,
    FetchQuest,
    AnsweringPhones,
    MAX_QUESTS
}

public class BaseQuest
{
    // Static Variables
    static private int kNextQuestID = 0;

    // Properties
    public string _questGiver { get; set; }
    public string _description { get; set; }
    public List<ItemType> _rewardItems { get; set; }
    public int _questID { get; set; }

    // Protected Variables
    protected bool _requirementsMet = false;

    public BaseQuest()
    {
        // Assign Quest ID
        _questID = kNextQuestID++;
    }

    // Static Interface
    static public BaseQuest CreateQuestFromType(QuestType type)
    {
        switch(type)
        {
            case QuestType.FlyerDelivery:
                return new FlyerDeliveryQuest();
            case QuestType.FetchQuest:
                return new FetchQuest();
            case QuestType.AnsweringPhones:
                return new AnsweringPhonesQuest();
            default:
                Debug.LogError("CreateQuestFromType: Unknown quest type!");
                return null;
        }
    }

    // Public Interface
    public string GetQuestInformation()
    {
        string information = _questGiver + ":\n" + "\t" + _description + "\n\tRewards: ";

        for(int i = 0; i < _rewardItems.Count; ++i)
        {
            information += ItemManager.Instance.GetItemDisplayName(_rewardItems[i]);
            if(i != (_rewardItems.Count - 1))
            {
                information += ", ";
            }
        }

        if(AreRequirementsMet() == true)
        {
            information += "\n\t\t\tREADY TO TURN IN!";
        }

        return information;
    }

    public void StartQuest(PlayerController playerController, GameObject[] questObjects)
    {
        OnQuestStarted(playerController, questObjects);
    }

    public void CompleteQuest(PlayerController playerController)
    {
        // Give the player the rewards
        Inventory inventory = playerController.GetInventory();
        foreach(ItemType rewardItem in _rewardItems)
        {
            if(inventory.AddItem(rewardItem, 1) == Inventory.kInventoryFull)
            {
                // Inventory was full, just drop the item on the ground
                ItemManager.Instance.SpawnItemAtLocation(rewardItem, playerController.transform.position);
            }
        }

        OnQuestCompleted(playerController);
        OnQuestFinished(playerController);
    }

    public void FailQuest(PlayerController playerController)
    {
        OnQuestFailed(playerController);
        OnQuestFinished(playerController);
    }

    virtual public bool AreRequirementsMet()
    {
        return _requirementsMet;
    }
    
    // Protected Interface
    virtual protected void OnQuestStarted(PlayerController playerController, GameObject[] questObjects)
    {

    }

    virtual protected void OnQuestCompleted(PlayerController playerController)
    {

    }

    virtual protected void OnQuestFailed(PlayerController playerController)
    {

    }

    virtual protected void OnQuestFinished(PlayerController playerController)
    {

    }

    virtual protected void OnRequirementsMet(PlayerController playerController)
    {

    }
}
