﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestManager : MonoBehaviour
{
    // Singleton
    public static QuestManager Instance { get; private set; }

    // Private Variables
    private PlayerController _playerController = null;
    private List<BaseQuest> _activeQuests;
    private List<BaseQuest> _completedQuests;
    private List<BaseQuest> _failedQuests;

    // Unity Interface
    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;

            // Find the player
            Instance._playerController = FindObjectOfType<PlayerController>();
            if(Instance._playerController == null)
            {
                // Should always be able to find the player
                Debug.LogError("QuestManager: Unable to find the PlayerController!");
            }

            // Initialize the lists
            Instance._activeQuests = new List<BaseQuest>();
            Instance._completedQuests = new List<BaseQuest>();
            Instance._failedQuests = new List<BaseQuest>();
        }
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    // Public Interface

    //! Returns the QuestID
    public int StartQuest(string questGiverName, QuestType quest, List<ItemType> rewardItems, GameObject[] questObjects = null)
    {
        BaseQuest newQuest = BaseQuest.CreateQuestFromType(quest);
        newQuest._questGiver = questGiverName;
        newQuest._rewardItems = rewardItems;

        // Trigger the start of the quest
        newQuest.StartQuest(_playerController, questObjects);

        // Keep track of the quest
        _activeQuests.Add(newQuest);

        return newQuest._questID;
    }

    public void CompleteQuest(int questID)
    {
        BaseQuest wantedQuest = FindQuestByID(questID);
        if(wantedQuest != null)
        {
            wantedQuest.CompleteQuest(_playerController);
        }

        Instance._completedQuests.Add(wantedQuest);
        Instance._activeQuests.Remove(wantedQuest);
    }

    public void FailQuest(int questID)
    {
        BaseQuest wantedQuest = FindQuestByID(questID);
        if(wantedQuest != null)
        {
            wantedQuest.FailQuest(_playerController);
        }

        Instance._failedQuests.Add(wantedQuest);
        Instance._activeQuests.Remove(wantedQuest);
    }

    public bool AreQuestRequirementsMet(int questID)
    {
        BaseQuest wantedQuest = FindQuestByID(questID);
        if(wantedQuest != null)
        {
            return wantedQuest.AreRequirementsMet();
        }

        return false;
    }

    public string GetActiveQuestDisplayInformation()
    {
        string information = "";

        foreach(BaseQuest quest in _activeQuests)
        {
            information += quest.GetQuestInformation() + "\n";
        }

        return information;
    }

    // Private Interface

    //! Finds a quest by it's ID. *Note:* Only searched through the active quests.
    BaseQuest FindQuestByID(int questID)
    {
        foreach(BaseQuest quest in _activeQuests)
        {
            if(quest._questID == questID)
            {
                return quest;
            }
        }

        Debug.LogError("No active quest found with the ID: " + questID);
        return null;
    }
}
