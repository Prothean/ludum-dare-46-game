﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnsweringPhonesQuest : BaseQuest
{
    // Constants
    public const int kNumberOfPhonesToAnswer = 3;

    // Private Variables
    private int _phonesAnswered = 0;
    private List<PhoneBooth> _phoneBooths = new List<PhoneBooth>();

    public AnsweringPhonesQuest()
    {
        _description = "Answer the phones that are ringing around the town for " + _questGiver + ". (" + kNumberOfPhonesToAnswer + "x Phones)";
    }

    // Protected Interface
    override protected void OnQuestStarted(PlayerController playerController, GameObject[] questObjects)
    {
        // Enable the Mailbox scripts on the quest items
        foreach(GameObject obj in questObjects)
        {
            PhoneBooth script = obj.GetComponent<PhoneBooth>();
            if(script != null)
            {
                _phoneBooths.Add(script);
                script.enabled = true;
            }
            else
            {
                Debug.LogError("AnsweringPhonesQuest: No `PhoneBooth` component found on one of the given quest objects.");
            }
        }

        if(questObjects.Length < kNumberOfPhonesToAnswer)
        {
            Debug.LogError("AnsweringPhonesQuest: Not enough phones defined to complete the quest!");
        }

        EventManager.Instance._OnPickUpPhoneEvents.Add(OnPhoneAnswered);
    }

    override protected void OnQuestFinished(PlayerController playerController)
    {
        // Unregister from the Event
        EventManager.Instance._OnPickUpPhoneEvents.Remove(OnPhoneAnswered);

        // Make sure all of the phonebooths have been turned off
        foreach(PhoneBooth phone in _phoneBooths)
        {
            phone.enabled = false;
        }
    }

    // Event Interface
    public void OnPhoneAnswered()
    {
        ++_phonesAnswered;

        if(_phonesAnswered >= kNumberOfPhonesToAnswer)
        {
            _requirementsMet = true;
        }
    }
}
