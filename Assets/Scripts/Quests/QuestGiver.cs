﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestGiver : InteractableItem
{
    // Enums
    private enum QuestState
    {
        NotStarted,
        InProgress,
        Completed
    }

    // Exposed Variables
    [SerializeField] private QuestType _quest;
    [SerializeField] private List<GameObject> _questObjects = new List<GameObject>();
    [SerializeField] private List<ItemType> _rewardItems = new List<ItemType>();
    [SerializeField] private string _giversName = "NULL";
    [SerializeField] private TextWriter _speechBubble;

    // Diaglogue Beats
    [SerializeField] private string _preQuestDialogue = "";
    [SerializeField] private string _startQuestDialogue = "";
    [SerializeField] private string _questInProgressDialogue = "";
    [SerializeField] private string _completeQuestDialogue = "";
    [SerializeField] private string _postQuestDialogue = "";

    // Private Variables
    private QuestState _questState = QuestState.NotStarted;
    private int _createdQuestID = -1;

    // Unity Interface
    void Start()
    {
        if(_giversName == "NULL")
        {
            // No given name, try to get the name from the AI script (if it exists)
            Generic_AI_Behaviour aiScript = GetComponent<Generic_AI_Behaviour>();
            if(aiScript != null)
            {
                _giversName = aiScript.CharacterName;
            }
        }

        string prefix = _giversName + ":\n";
        _preQuestDialogue = prefix + _preQuestDialogue;
        _startQuestDialogue = prefix + _startQuestDialogue;
        _questInProgressDialogue = prefix + _questInProgressDialogue;
        _completeQuestDialogue = prefix + _completeQuestDialogue;
        _postQuestDialogue = prefix + _postQuestDialogue;

        _speechBubble.setText(_preQuestDialogue);
    }

    // InteractableItem Interface
    override public InteractableType Interact(PlayerController player, Animator playerAnimator)
    {
        // Turn of UI
        DisplayUIPrompt(false);

        // Player can no longer interact with it
        player.SetItemInRange(null);

        switch(_questState)
        {
            case QuestState.NotStarted:
                // Start the quest
                _createdQuestID = QuestManager.Instance.StartQuest(_giversName, _quest, _rewardItems, _questObjects.Count > 0 ? _questObjects.ToArray() : null);

                _speechBubble.setText(_startQuestDialogue);
                _speechBubble.SetNextText(_questInProgressDialogue);

                _questState = QuestState.InProgress;
                return InteractableType.QuestStarted;
            case QuestState.InProgress:
                // Check if the player has completed the quest
                if(QuestManager.Instance.AreQuestRequirementsMet(_createdQuestID) == true)
                {
                    QuestManager.Instance.CompleteQuest(_createdQuestID);

                    _speechBubble.setText(_completeQuestDialogue);
                    _speechBubble.SetNextText(_postQuestDialogue);

                    _questState = QuestState.Completed;
                    return InteractableType.QuestHandedIn;
                }
                else
                {
                    // TODO: Some feedback about the quest not being done?
                    //_speechBubble.setText(_questInProgressDialogue);
                }

                break;
        }

        return InteractableType.None;
    }
}
