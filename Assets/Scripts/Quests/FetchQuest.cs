﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FetchQuest : BaseQuest
{
    // Private Variables
    private Dictionary<ItemType, int> _itemsNeeded = new Dictionary<ItemType, int>();
    private PlayerController _playerController = null;

    // Protected Interface
    override protected void OnQuestStarted(PlayerController playerController, GameObject[] questObjects)
    {
        _playerController = playerController;

        // Enable the Mailbox scripts on the quest items
        foreach(GameObject obj in questObjects)
        {
            ItemPickup script = obj.GetComponent<ItemPickup>();
            if(script != null)
            {
                ItemType itemType = script.GetItemType();
                if(_itemsNeeded.ContainsKey(itemType))
                {
                    _itemsNeeded[itemType] += 1;
                }
                else
                {
                    _itemsNeeded.Add(itemType, 1);
                }
            }
            else
            {
                Debug.LogError("FetchQuest: No `ItemPickup` component found on one of the given quest objects.");
            }
        }

        _description = "Retrieve the following items: ";
        int count = 0;
        foreach(KeyValuePair<ItemType, int> item in _itemsNeeded)
        {
            _description += ItemManager.Instance.GetItemDisplayName(item.Key) + "(x" + item.Value + ")";

            if(count < (_itemsNeeded.Count - 1))
            {
                _description += ", ";
            }
        }
    }

    override protected void OnQuestCompleted(PlayerController playerController)
    {
        // Remove the items from the player's inventory
        Inventory playerInventory = playerController.GetInventory();
        foreach(KeyValuePair<ItemType, int> item in _itemsNeeded)
        {
            playerInventory.RemoveItemByType(item.Key, item.Value);
        }
    }

    override public bool AreRequirementsMet()
    {
        _requirementsMet = true;

        Inventory playerInventory = _playerController.GetInventory();
        foreach(KeyValuePair<ItemType, int> item in _itemsNeeded)
        {
            if(playerInventory.ContainsItem(item.Key, item.Value) == false)
            {
                _requirementsMet = false;
                return false;
            }
        }

        return _requirementsMet;
    }
}
