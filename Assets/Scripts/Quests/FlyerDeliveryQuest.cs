﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyerDeliveryQuest : BaseQuest
{
    // Constants
    public const int kWantedDeliveryCount = 4;

    // Private Variables
    private int _flyersDelivered = 0;

    public FlyerDeliveryQuest()
    {
        _description = "Deliver the flyers from " + _questGiver + " to the mailboxes of " + kWantedDeliveryCount + " people.";
    }

    // Protected Interface
    override protected void OnQuestStarted(PlayerController playerController, GameObject[] questObjects)
    {
        // Enable the Mailbox scripts on the quest items
        foreach(GameObject obj in questObjects)
        {
            Mailbox script = obj.GetComponent<Mailbox>();
            if(script != null)
            {
                script.enabled = true;
            }
            else
            {
                Debug.LogError("FlyerDeliveryQuest: No `Mailbox` component found on one of the given quest objects.");
            }
        }

        EventManager.Instance._OnDeliverMailEvents.Add(OnFlyerDelivered);
    }

    override protected void OnQuestFinished(PlayerController playerController)
    {
        // Unregister from the Event
        EventManager.Instance._OnDeliverMailEvents.Remove(OnFlyerDelivered);
    }

    // Event Interface
    public void OnFlyerDelivered()
    {
        ++_flyersDelivered;

        if(_flyersDelivered >= kWantedDeliveryCount)
        {
            _requirementsMet = true;
        }
    }
}
