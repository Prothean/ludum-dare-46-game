﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    // Singleton
    public static EventManager Instance { get; private set; }

    // Enums
    public enum EventType
    {
        ItemPickup,
        ItemDrop,
        DelieverMail
    }

    // Event Callbacks
    public delegate void OnGameStart();
    public delegate void OnItemPickup(ItemType item, int index, int totalAmount);
    public delegate void OnItemDrop(ItemType item, int index, int remainingAmount);
    public delegate void OnInventoryIndexChanged(int index, ItemType itemAtIndex);
    public delegate void OnEnergyLevelChanged(float eneryAmount);
    public delegate void OnEnergyFullyDrained();
    public delegate void OnInteractMsg(string message);
    public delegate void OnDeliverMail();
    public delegate void OnPickupPhone();
    public delegate void OnTimerEnd();

    // Structs
    private class TimedEvent
    {
        public TimedEvent(OnTimerEnd callback, float time)
        {
            _callback = callback;
            _time = time;
        }

        public bool Tick()
        {
            _time -= Time.deltaTime;
            return (_time <= 0.0f);
        }

        public OnTimerEnd _callback;
        public float _time;
    }

    public List<OnGameStart> _OnGameStartEvents { get; private set; }
    public List<OnItemPickup> _OnItemPickupEvents { get; private set; }
    public List<OnItemDrop> _OnItemDropEvents { get; private set; }
    public List<OnInventoryIndexChanged> _OnInventoryIndexChangedEvents { get; private set; }
    public List<OnEnergyLevelChanged> _OnEnergyLevelChangedEvents { get; private set; }
    public List<OnEnergyFullyDrained> _OnEnergyFullyDrainedEvents { get; private set; }
    public List<OnInteractMsg> _OnInteractMessageEvents { get; private set; }
    public List<OnDeliverMail> _OnDeliverMailEvents { get; private set; }
    public List<OnPickupPhone> _OnPickUpPhoneEvents { get; private set; }

    // Private Variables
    private List<TimedEvent> _TimedEvents;

    // Unity Interface
    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }

        Instance._OnGameStartEvents = new List<OnGameStart>();
        Instance._OnItemPickupEvents = new List<OnItemPickup>();
        Instance._OnItemDropEvents = new List<OnItemDrop>();
        Instance._OnInventoryIndexChangedEvents = new List<OnInventoryIndexChanged>();
        Instance._OnEnergyLevelChangedEvents = new List<OnEnergyLevelChanged>();
        Instance._OnEnergyFullyDrainedEvents = new List<OnEnergyFullyDrained>();
        Instance._OnInteractMessageEvents = new List<OnInteractMsg>();
        Instance._OnDeliverMailEvents = new List<OnDeliverMail>();
        Instance._OnPickUpPhoneEvents = new List<OnPickupPhone>();
        Instance._TimedEvents = new List<TimedEvent>();
    }

    private void OnDestroy()
    {
        Instance = null;
    }

    void Update()
    {
        for(int i = 0; i < Instance._TimedEvents.Count; ++i)
        {
            if(Instance._TimedEvents[i].Tick())
            {
                // Trigger the callback
                Instance._TimedEvents[i]._callback();

                // Remove the timed event
                Instance._TimedEvents.RemoveAt(i);
                --i;
            }
        }
    }

    // Public Interface
    public void AddTimedEvent(OnTimerEnd eventCallback, float timer)
    {
        Instance._TimedEvents.Add(new TimedEvent(eventCallback, timer));
    }

    public void TriggerOnGameStartEvent()
    {
        foreach(OnGameStart callback in Instance._OnGameStartEvents)
        {
            callback();
        }
    }

    public void TriggerOnItemPickupEvent(ItemType item, int index, int totalAmount)
    {
        foreach(OnItemPickup callback in Instance._OnItemPickupEvents)
        {
            callback(item, index, totalAmount);
        }
    }

    public void TriggerOnInventoryIndexChangedEvent(int index, ItemType itemAtIndex)
    {
        foreach(OnInventoryIndexChanged callback in Instance._OnInventoryIndexChangedEvents)
        {
            callback(index, itemAtIndex);
        }
    }

    public void TriggerOnItemDropEvent(ItemType item, int index, int remainingAmount)
    {
        foreach(OnItemDrop callback in Instance._OnItemDropEvents)
        {
            callback(item, index, remainingAmount);
        }
    }

    public void TriggerOnEnergyLevelChangedEvent(float energyAmount)
    {
        foreach(OnEnergyLevelChanged callback in Instance._OnEnergyLevelChangedEvents)
        {
            callback(energyAmount);
        }
    }

    public void TriggerOnEnergyFullyDrainedEvent()
    {
        foreach(OnEnergyFullyDrained callback in Instance._OnEnergyFullyDrainedEvents)
        {
            callback();
        }
    }

    public void TriggerOnInteractMessageEvent(string message)
    {
        foreach(OnInteractMsg callback in Instance._OnInteractMessageEvents)
        {
            callback(message);
        }
    }

    public void TriggerOnDeliverMailEvent()
    {
        foreach(OnDeliverMail callback in Instance._OnDeliverMailEvents)
        {
            callback();
        }
    }

    public void TriggerOnPickUpPhoneEvent()
    {
        foreach(OnPickupPhone callback in Instance._OnPickUpPhoneEvents)
        {
            callback();
        }
    }
}
