﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationTest : MonoBehaviour
{
    // Enums
    public enum SupportedAnimations
    {
        Standing,
        Walking,
        Jogging,
        Running,
        Sitting
    }
    // Public Variables
    public SupportedAnimations _wantedAnimation = SupportedAnimations.Standing;

    // Private Variables
    private SupportedAnimations _currentAnimation = SupportedAnimations.Standing;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if(_wantedAnimation != _currentAnimation)
        {
            _currentAnimation = _wantedAnimation;

            Animator animator = GetComponent<Animator>();
            animator.Play(_currentAnimation.ToString());
        }
    }
}
