﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CharacterAnimationStates
{
    Standing,
    Sitting,
    Walking,
    Jogging,
    Running
}
